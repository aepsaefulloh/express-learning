const db = require("../models");
const Content = db.contents;
const Op = db.Sequelize.Op;

// Create and Save a new Content
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            message: "title tidak boleh kosong"
        });
        return;
    }
    // Create a Content
    const content = {
        title: req.body.title,
        subtitle: req.body.subtitle,
        content: req.body.content,
        images: req.body.images,
        keyword: req.body.keyword,
        summary: req.body.summary,
        published: req.body.published ? req.body.published : false
    };
    // Save Content in the database
    Content.create(content)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Content."
            });
        });
};
// Retrieve all Contents from the database.
exports.findAll = (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
    Content.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving contents."
            });
        });
};
// Find a single Content with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Content.findByPk(id)
        .then(data => {
            if (data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `Cannot find Content with id=${id}.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Content with id=" + id
            });
        });
};
// Update a Content by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    Content.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Content was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Content with id=${id}. Maybe Content was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Content with id=" + id
            });
        });
};
// Delete a Content with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Content.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Content was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Delete with id=${id}. Maybe Content was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Delete with id=" + id
            });
        });
};
// Delete all Contents from the database.
exports.deleteAll = (req, res) => {
    Content.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} content were contentd successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all contents."
            });
        });
};
// Find all published Contents
exports.findAllPublished = (req, res) => {
    Content.findAll({ where: { published: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving contents."
            });
        });
};
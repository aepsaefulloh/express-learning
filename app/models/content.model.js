module.exports = (sequelize, Sequelize) => {
    const Content = sequelize.define("content", {
        title: {
            type: Sequelize.STRING
        },
        subtitle: {
            type: Sequelize.STRING
        },
        content: {
            type: Sequelize.STRING
        },
        images: {
            type: Sequelize.STRING
        },
        keyword: {
            type: Sequelize.STRING
        },
        summary: {
            type: Sequelize.STRING
        },
        published: {
            type: Sequelize.BOOLEAN
        }
    });
    return Content;
};